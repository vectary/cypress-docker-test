FROM cypress/browsers:node12.14.1-chrome85-ff81
ARG iapp=apt

RUN apt-get --allow-releaseinfo-change update
RUN apt install -y git python3 libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1

RUN npm install yarn -g --force
RUN yarn init -2

RUN npm install -g electron

COPY .yarn /app/.yarn
COPY .yarn* /app/
COPY yarn.lock /app/
COPY *.json /app/
COPY cypress /app/cypress
COPY cypress.json /app/

RUN cat /etc/os-release

WORKDIR "/app/"
RUN yarn
# RUN chmod 777 -R /root && \
#     chmod 777 -R node_modules/
RUN yarn run e2e
