/// <reference types="cypress" />

describe('example to-do app', () => {
    beforeEach(() => {
        cy.clearLocalStorage();
        cy.clearCookies();
        cy.visit('https://www.vectary.com/');
        cy.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from failing the test
            return false;
        });
    })

    // const signUp = (name: string, email: string, password: string) => {
    const signUp = () => {
        cy.get('#onetrust-accept-btn-handler').click();
        cy.get('a.btn-primary.mar-left.nav.w-inline-block').click();
        cy.contains('Email').click();
        cy.location('pathname').should('include', 'signup');

        // cy.get('label.registration_form-name > input[type=text]')
        //     .type(name).should('have.value', name);
        // cy.get('input[type=email] ')
        //     .type(email).should('have.value', email);
        // cy.get('input[type=password] ')
        //     .type(password).should('have.value', password);

        // cy.get('#ToU').click();
        // cy.get('#PP').click();
        // cy.contains('Sign up').click();
    }

    it('sign up', () => {
        // const email = 'email' + Math.random() + '@random' + Math.random() + '.hu';
        signUp();
        /* ==== Generated with Cypress Studio ==== */
        // cy.get('.sc-kIeSZW > .sc-dlfnuX').click();
        // cy.get('.sc-iwyWTf').click();
        // /* ==== End Cypress Studio ==== */
    })

    // it('create new workspace', () => {
    //     const email = 'email' + Math.random() + '@random' + Math.random() + '.hu';
    //     signUp(email, email, email);

    //     cy.get('li.sc-bTvSiT.fIStpw').click();
    //     cy.contains('Create Workspace').click();
    //     cy.url().should('include', 'workspaceId');
    // })
})
